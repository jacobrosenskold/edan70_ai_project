"""
Preprocessing of hand key point data sets.
"""

import numpy as np
import random
from keras import utils
import glob
import pickle
import os


def create_datasets(all_pose_coords, classes, use_keypoint_indexes, window_size, train_on, train_dividable_with=1):
    X = []
    Y = []
    for i, pose_coords in enumerate(all_pose_coords):
        for window in pose_coords:
            X.append(window[-window_size:])
            Y.append(classes[i])

    # Flatten innermost coordinate array and remove confidence score.
    # Only extract keypoints in 'use_keypoint_indexes'.
    X_flattened_coords = []
    for window_index, window in enumerate(X):
        flattened_window = []
        for frame in window:
            coords = []
            for i in use_keypoint_indexes:
                coord_conf = frame[i]
                x = coord_conf[0]
                y = coord_conf[1]
                coords.append(x)
                coords.append(y)
            flattened_window.append(coords)
        X_flattened_coords.append(np.array(flattened_window))

    X, Y = shuffle(X_flattened_coords, Y)

    # Split train/test/validate
    train_size = round(len(X) * train_on)
    train_size = int(np.ceil(train_size / train_dividable_with) * train_dividable_with)

    if train_size % 2 != 0:
        train_size += 1

    test_validate_size = int((len(X) - train_size) / 2)

    X_train = X[:train_size]
    Y_train = Y[:train_size]
    X_test = X[train_size:train_size + test_validate_size]
    Y_test = Y[train_size:train_size + test_validate_size]
    X_valid = X[-test_validate_size:]
    Y_valid = Y[-test_validate_size:]

    Y_train = utils.to_categorical(Y_train)
    Y_valid = utils.to_categorical(Y_valid)

    return (X_train, Y_train), (X_test, Y_test), (X_valid, Y_valid)


def shuffle(X, Y):
    X_shuffled = []
    Y_shuffled = []
    random_indexes = random.sample(range(0, len(X)), len(X))
    for i in random_indexes:
        X_shuffled.append(X[i])
        Y_shuffled.append(Y[i])

    return np.array(X_shuffled), np.array(Y_shuffled)


def prepare_data(window_size, use_key_points, train_on=0.7, batch_size=1):

    grip_coords_dir = '../../data/pose_coordinates/grip/32/'
    drop_coords_dir = '../../data/pose_coordinates/drop/32/'

    if not os.path.isdir(grip_coords_dir):
        print('Cannot find ' + grip_coords_dir)
        exit(1)

    if not os.path.isdir(drop_coords_dir):
        print('Cannot find ' + drop_coords_dir)
        exit(1)

    all_pose_coords = []
    grip_coord_paths = glob.glob(grip_coords_dir + '*.p')

    classes = [0] * len(grip_coord_paths)
    for grip_coord_path in grip_coord_paths:
        all_pose_coords.append(pickle.load(open(grip_coord_path, 'rb')))

    drop_coord_paths = glob.glob(drop_coords_dir + '*.p')
    classes += [1] * len(drop_coord_paths)
    for drop_coord_path in drop_coord_paths:
        all_pose_coords.append(pickle.load(open(drop_coord_path, 'rb')))

    train, test, validate = create_datasets(all_pose_coords,
                                            classes,
                                            use_key_points,
                                            window_size=window_size,
                                            train_on=train_on,
                                            train_dividable_with=batch_size
                                            )

    print('Train:', len(train[0]))
    print('Test:', len(test[0]))
    print('Validate:', len(validate[0]))

