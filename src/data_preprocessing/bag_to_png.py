"""
Used for selecting images in bag files and saving them as png images.
This was used when collecting images of the different stop button parts.
The images was used for training of YOLO.
"""

import pyrealsense2 as rs
import cv2
import numpy as np
import os


def bag_to_png(bag_path, target_dir):
    if not os.path.isdir(target_dir):
        os.mkdir(target_dir)


    saved_idx = 0
    info_path = target_dir + 'info.txt'
    if os.path.isfile(info_path):
        with open(info_path, 'r') as f:
            saved_idx = int(f.read())

    print('Saved idx', saved_idx)


    rs_pipeline = rs.pipeline()
    config = rs.config()
    config.enable_device_from_file(bag_path, repeat_playback=False)
    profile = rs_pipeline.start(config)
    device = profile.get_device()
    playback = rs.playback(device)
    playback.set_real_time(False)

    frame_idx = 0
    try:
        while True:
            success, frames = rs_pipeline.try_wait_for_frames()

            if not success:
                break

            color_frame = frames.get_color_frame()
            color_image = np.asanyarray(color_frame.get_data())

            img = color_image
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGBA)

            img_copy = img.copy()
            cv2.putText(img_copy, 'Frame: ' + str(frame_idx), (20, 20), cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 0), 2)
            cv2.imshow('Stream', img_copy)

            key = cv2.waitKey(100)

            if key == 27:
                # esc => exit
                break
            elif key == 32:
                # space => paus stream
                key = cv2.waitKey(-1)
                if key == 99:  # c => save image
                    save_frame(img, saved_idx, frame_idx)
                    saved_idx += 1
            elif key == 99: # c => save image
                save_frame(img, saved_idx, frame_idx)
                saved_idx += 1

            frame_idx += 1
    finally:
        cv2.destroyAllWindows()
        rs_pipeline.stop()
        if os.path.isfile(info_path):
            os.remove(info_path)
        with open(info_path, 'w') as f:
            f.write(str(saved_idx))


def save_frame(img, saved_idx, frame_idx):
    img_path = target_dir + 'frame' + f"{saved_idx:04d}" + '.png'
    cv2.imwrite(img_path, img)
    print(f'Saved frame {frame_idx:05d} (Saved index f{saved_idx:04d})')


if __name__ == '__main__':
    bag_path = '../../data/bag_files/new_depth_3.bag'
    target_dir = '../../data/hand_images/'

    bag_to_png(bag_path, target_dir)
