"""
Used for image augmentation (data set for YOLO).
"""

from imgaug import augmenters as iaa
from imgaug.augmenters import Sometimes, Add, CoarseDropout, Sharpen
from imgaug.augmentables.bbs import BoundingBox, BoundingBoxesOnImage
import cv2
import numpy as np
import os
from shutil import copyfile

seq = iaa.Sequential([
    iaa.Fliplr(0.5),
    iaa.Crop(percent=(0, 0.1)),
    iaa.Sometimes(
        0.5,
        iaa.GaussianBlur(sigma=(0, 0.5))
    ),
    iaa.LinearContrast((0.75, 1.5)),
    iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05 * 255), per_channel=0.5),
    iaa.Multiply((0.8, 1.2), per_channel=0.2),
    Sometimes(0.20, iaa.Affine(
        scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},
        translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)},
        rotate=(-25, 25),
        shear=(-8, 8)
    )),
    Sometimes(0.5, CoarseDropout(p=0.1, size_percent=0.02)),
    Sometimes(0.5, Add((-30, 30), per_channel=0.3)),
    Sometimes(0.20, Sharpen(alpha=(0, 1.0), lightness=(0.75, 1.5))),
    Sometimes(0.20, iaa.PiecewiseAffine(scale=(0.01, 0.05)))
], random_order=True)


def main(image_dir, target_dir, bbox_dir, n_augs, test_on=0.15, validate_on=0.15):
    train_dir = target_dir + 'train/'
    test_dir = target_dir + 'test/'
    validation_dir = target_dir + 'validate/'

    image_names = os.listdir(image_dir)
    train_images, test_images, validation_images = split_data(image_names, test_on, validate_on)
    save_test_images(test_images, image_dir, bbox_dir, test_dir)
    save_test_images(validation_images, image_dir, bbox_dir, validation_dir)

    images_all = []
    bboxes_all = []
    j = 0
    save_image_idx = 0
    for img_name in train_images:
        img_name = img_name.replace('._', '')  # linux stuff
        if not img_name.endswith('.png'):
            continue
        img = cv2.imread(image_dir + img_name)
        bboxes = read_bbox_from_file(bbox_dir + img_name.replace('.png', '.txt'))

        # True, None, None
        imgs_aug, bboxes_aug = augment(img, bboxes, n_augs)
        imgs_aug.append(img)
        bboxes_aug.append(bboxes)
        images_all += imgs_aug
        bboxes_all += bboxes_aug

        j += 1
        if j % 50 == 0:
            save_image_idx = save_train_images(images_all, bboxes_all, train_dir, save_image_idx)
            images_all = []
            bboxes_all = []

    save_train_images(images_all, bboxes_all, train_dir, save_image_idx)


def save_test_images(test_images, image_source_dir, bbox_dir, test_dir):
    images_dir = test_dir + '/images/'
    labels_dir = test_dir + '/labels/'

    if not os.path.isdir(test_dir):
        os.mkdir(test_dir)
        os.mkdir(images_dir)
        os.mkdir(labels_dir)

    for image_name in test_images:
        image_name = image_name.replace('._', '')  # linux stuff
        if image_name.endswith('.png'):
            copyfile(image_source_dir + image_name, images_dir + image_name)
            bbox_name = image_name.replace('.png', '.txt')
            copyfile(bbox_dir + bbox_name, labels_dir + bbox_name)


def save_train_images(imgs, bboxes, dir, image_idx):
    images_dir = dir + '/images/'
    labels_dir = dir + '/labels/'

    if not os.path.isdir(dir):
        os.mkdir(dir)
        os.mkdir(images_dir)
        os.mkdir(labels_dir)

    if len(imgs) == 0:
        return image_idx

    # Save
    for i, img in enumerate(imgs):
        img_save_path = f'{images_dir}frame{image_idx:04d}.png'
        label_save_path = f'{labels_dir}frame{image_idx:04d}.txt'
        image_idx += 1
        bboxes_img = bboxes[i]
        cv2.imwrite(img_save_path, img)

        with open(label_save_path, 'a') as bbox_file:
            for i, bbox in enumerate(bboxes_img):
                bbox_file.write(' '.join([str(e) for e in bbox]))
                if i != len(bboxes_img) - 1:
                    bbox_file.write('\n')

    print(f'Saved image index {image_idx - len(imgs)} - {image_idx} ({len(imgs)} images).')
    return image_idx


def split_data(images, test_on, validate_on):
    np.random.shuffle(images)
    n = len(images)

    test_on = int(n * test_on)
    validate_on = int(n * validate_on)
    train_on = n - test_on - validate_on

    train = images[:train_on]
    test = images[train_on:train_on + test_on]
    validate = images[-validate_on:]

    print(f'Split into: training: {len(train)}, testing: {len(test)}, validation: {len(validate)}.')

    return train, test, validate


def read_bbox_from_file(bbox_path):
    with open(bbox_path, 'r') as bbox_file:
        bboxes = bbox_file.read().split('\n')

    bboxes_parsed = []
    for bbox in bboxes:
        bbox = [float(e) for e in bbox.split()]
        bbox[0] = int(bbox[0])
        bboxes_parsed.append(bbox)

    return bboxes_parsed


def bboxes_to_img_coords(img, bboxes):
    img_h, img_w, _ = img.shape
    bboxes_img_coords = []
    for bbox in bboxes:
        c, x_center, y_center, w, h = bbox
        bboxes_img_coords.append([c,
                                  int(x_center * img_w),
                                  int(y_center * img_h),
                                  int(w * img_w),
                                  int(h * img_h)])

    return bboxes_img_coords


def normalize_bboxes(img, bboxes):
    img_h, img_w, _ = img.shape
    normalized_bboxes = []
    for i in range(len(bboxes)):
        bboxes_img = []
        for j in range(len(bboxes[i])):
            c, x_center, y_center, w, h = bboxes[i][j]
            bboxes_img.append([c,
                           x_center / img_w,
                           y_center / img_h,
                           w / img_w,
                           h / img_h])

        normalized_bboxes.append(bboxes_img)

    return normalized_bboxes


def create_imgaug_bboxes(image, bboxes):
    imgaug_bboxes = []
    for bbox in bboxes:
        c, x_center, y_center, w, h = bbox
        x = int(x_center - w / 2)
        y = int(y_center - h / 2)
        imgaug_bboxes.append(BoundingBox(label=c, x1=x, y1=y, x2=x + w, y2=y + h))
    img_h, img_w, _ = image.shape
    return BoundingBoxesOnImage(imgaug_bboxes, shape=(img_h, img_w))


def convert_imgaug_bboxes_to_arrays(imgaug_bboxes):
    array_bboxes = []
    for imgaug_bbox in imgaug_bboxes:
        width = int(imgaug_bbox.width)
        height = int(imgaug_bbox.height)
        array_bboxes.append([
            imgaug_bbox.label,
            imgaug_bbox.x1_int + width / 2,
            imgaug_bbox.y1_int + height / 2,
            width,
            height
        ])

    return array_bboxes


def augment(img, bboxes, n_augs):
    bboxes_img_coords = bboxes_to_img_coords(img, bboxes)

    imgs_aug = [np.copy(img)] * n_augs
    imgaug_bboxes = [create_imgaug_bboxes(img, bboxes_img_coords)] * n_augs

    imgs_aug, imgaug_bboxes = seq(images=imgs_aug, bounding_boxes=imgaug_bboxes)

    # Convert bboxes back from BoundingBox to arrays
    array_bboxes = []
    for i in range(len(imgaug_bboxes)):

        # Remove/clip bboxes out of image
        imgaug_bboxes[i] = imgaug_bboxes[i].remove_out_of_image(fully=True, partly=False)
        imgaug_bboxes[i] = imgaug_bboxes[i].clip_out_of_image()

        array_bboxes.append(convert_imgaug_bboxes_to_arrays(imgaug_bboxes[i]))

    normalized_bboxes = normalize_bboxes(img, array_bboxes)
    return imgs_aug, normalized_bboxes


if __name__ == '__main__':
    image_dir = '../../data/raw_with_hands/all/'
    bbox_dir = '../../data/bboxes_hands/'
    target_dir = '../../data/all_hands_and_aug/'
    n_augs = 5

    main(image_dir, target_dir, bbox_dir, n_augs, test_on=0.15, validate_on=0.15)
