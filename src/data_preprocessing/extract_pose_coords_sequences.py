"""
Used for
1) Annotating where in bag files there are hand activities.
2) Running OpenPose and extracting hand key points sequences of 32 frames each.
"""

from image_handler import ImageHandler
from real_sense_stream import RealSenseStream
from pose_estimator import PoseEstimator
import os
import pickle
import sys

LEFT_HAND_LABEL = 'left_hand'
RIGHT_HAND_LABEL = 'right_hand'
HAND_LABELS = {LEFT_HAND_LABEL, RIGHT_HAND_LABEL}
FRAME_WINDOW_BASE_SIZE = 32
WINDOW_MULTIPLIER = 1
HAND_NET_RESOLUTION = PoseEstimator.DEFAULT_HAND_NET_RESOLUTION
HAND_BBOX_SCALE_FACTOR = ObjectDetector.DEFAULT_SQUARE_BBOX_SCALE_FACTOR

classes = {
    0: 'grip',
    1: 'drop',
}


def extract_pose_coords(stream, object_detection_results, selected_frames, frame_window_size, use_left_hand):
    pose_coords = []
    pose_estimator = PoseEstimator(hand_net_resolution=HAND_NET_RESOLUTION)

    frame_count = 0
    n_extracted_frames = 0
    save_frames_left = 0
    pose_coords_idx = 0
    acc_conf = 0
    try:
        stream_pipe = stream.start()

        while True:
            success, frames = stream_pipe.try_wait_for_frames()
            if not success:
                break

            valid, depth_frame, color_frame = stream.aligned_frames(frames)
            if not valid:
                continue

            color_image = stream.frame_to_image(color_frame)

            save_frames_to = frame_count + frame_window_size - 1
            if save_frames_to in selected_frames:
                save_frames_left = frame_window_size

            if save_frames_left > 0:
                save_frame_idx = frame_window_size - save_frames_left
                if save_frame_idx == 0:
                    pose_coords.append([])
                object_detection_result = object_detection_results[frame_count]
                hand_rectangles = create_hand_rectangles(object_detection_result)
                pose_estimator.pose_estimate(color_image, hand_rectangles)

                if use_left_hand:
                    key_points = pose_estimator.left_hand_points()
                else:
                    key_points = pose_estimator.right_hand_points()

                for kp in key_points:
                    acc_conf += kp[2]
                pose_coords[pose_coords_idx].append(key_points)
                n_extracted_frames += 1

                save_frames_left -= 1
                if save_frames_left == 0:
                    if pose_coords_idx % 10 == 0:
                        print(f'Estimating window index: {pose_coords_idx}.')
                    sys.stdout.flush()
                    pose_coords_idx += 1

            frame_count += 1
    finally:
        stream.stop()

    return pose_coords


def create_hand_rectangles(object_detection_result):
    hands = []
    count = 0
    for r in object_detection_result:
        label, prob, x, y, w, h = ObjectDetector.extract_detection_result(
            r,
            make_sides_equal_for=HAND_LABELS,
            width_factor=HAND_BBOX_SCALE_FACTOR,
            height_factor=HAND_BBOX_SCALE_FACTOR
        )
        if label in HAND_LABELS:
            hands.append((label, PoseEstimator.create_hand_rectangle(x, y, w, h)))
            count += 1
            if count == 2:
                break

    return PoseEstimator.create_hand_rectangles(hands)


def select_frames(bag_path):
    image_handler = ImageHandler('Select frames')
    stream = RealSenseStream(bag_path)

    image_handler.show_window()
    n_classes = 2
    selected_frames = [set() for _ in range(n_classes)]
    key = -1
    try:
        stream_pipe = stream.start()

        while True:
            success, frames = stream_pipe.try_wait_for_frames()
            if not success:
                break

            valid, depth_frame, color_frame = stream.aligned_frames(frames)
            if not valid:
                continue

            color_image = stream.frame_to_image(color_frame)
            image_handler.set_image(color_image, convert_brg2rgb=True)

            if key == 115:
                # 's' => step through the frames one at a time.
                key = image_handler.show_image(0)
            else:
                key = image_handler.show_image(150)

            if key == 27:
                # esc => exit
                break
            elif key == 32:
                # space => paus
                key = image_handler.wait_for_key(0)
            elif key in (48, 49): # 0, 1
                # Capture frame if c is pressed
                if key == 48:  # class 0
                    c = 0
                elif key == 49:  # class 1
                    c = 1
                else:
                    continue

                selected_frames[c].add(image_handler.frame_count())
                latest_saved_frame = image_handler.frame_count()
                print(f'Saved frame: {latest_saved_frame} ({classes[c]}). (nbr saved: {len(selected_frames[c])})')

    finally:
        image_handler.close()
        stream.stop()

    return selected_frames


def run_select_frames(bag_path, save_dir, save_idx, load_frames_from_file=False):
    selected_frames_save_path = f'{save_dir}frames_{save_idx}.p'
    if load_frames_from_file:
        print(f'Load selected frames from: {selected_frames_save_path}')
        selected_frames = pickle.load(open(selected_frames_save_path, 'rb'))
    else:
        if os.path.isfile(selected_frames_save_path):
            print(f'Frame path {selected_frames_save_path} already exists.')
            exit(1)
        selected_frames = select_frames(bag_path)
        for i in range(len(selected_frames)):
            c = classes[i]
            selected_frames_save_path = f'{save_top_dir}{c}/frames_{save_idx}.p'
            frames = selected_frames[i]
            if len(frames) > 0:
                pickle.dump(frames, open(selected_frames_save_path, 'wb'))
                print(f'Saved frames to:\n{selected_frames_save_path}')

    return selected_frames


def run_pose_extraction(bag_path, selected_frames, save_dir, save_idx, use_left_hand):
    stream = RealSenseStream(bag_path)
    object_detection_results = run_object_detection(stream)

    sys.stdout.flush()
    frame_window_size = FRAME_WINDOW_BASE_SIZE * WINDOW_MULTIPLIER
    pose_coords = extract_pose_coords(stream, object_detection_results, selected_frames, frame_window_size,
                                      use_left_hand)

    class_and_window_size_dir = f'{save_dir}{frame_window_size}/'
    if not os.path.isdir(class_and_window_size_dir):
        os.mkdir(class_and_window_size_dir)
    save_path = f'{class_and_window_size_dir}coords_{save_idx}.p'
    if os.path.isfile(save_path):
        print(f'Pose coords path {save_path} already exists.')
        exit(1)

    pickle.dump(pose_coords, open(save_path, 'wb'))
    print(f'Saved pose coords to {save_path}.')
    print()
    sys.stdout.flush()


def run_object_detection(stream):
    object_detector = ObjectDetector()
    object_detection_results = []
    try:
        stream_pipe = stream.start()

        i = 0
        while True:
            success, frames = stream_pipe.try_wait_for_frames()
            if i % 400 == 0:
                print(f'Object detection frame {i}')
                sys.stdout.flush()
            if not success:
                break

            valid, depth_frame, color_frame = stream.aligned_frames(frames)
            if not valid:
                continue

            color_image = stream.frame_to_image(color_frame)
            object_detector.detect_array_img(color_image)
            object_detection_results.append(object_detector.best_detection_results())

            i += 1
    except Exception as e:
        print(e)
    finally:
        print(f'Object detection frame {i}')
        stream.stop()

    print('Object detection done.\n')
    return object_detection_results


if __name__ == '__main__':
    save_top_dir = '../../data/pose_coordinates/new_classes/'

    bag_paths_select_frames = [
        '../../data/bag_files/new_classes/open_hand_right_1.bag',
    ]

    bag_paths = [
        '../../data/bag_files/new_classes/finger_right_1.bag',
        '../../data/bag_files/new_classes/finger_right_2.bag',
    ]

    bag_index = int(sys.argv[1])
    c = int(sys.argv[2])
    frame_start_index = 0

    # *** Extract poses ***
    from object_detector import ObjectDetector
    import regex as re

    HAND_NET_RESOLUTION = PoseEstimator.DEFAULT_HAND_NET_RESOLUTION
    HAND_BBOX_SCALE_FACTOR = ObjectDetector.DEFAULT_SQUARE_BBOX_SCALE_FACTOR
    # HAND_NET_RESOLUTION = 416
    # HAND_BBOX_SCALE_FACTOR = 3
    log = open('extract_new_classes.log', 'a')
    sys.stdout = log
    save_dir = save_top_dir + classes[c] + '/'
    if not os.path.isdir(save_dir):
        os.mkdir(save_dir)

    save_index = frame_start_index + bag_index
    bag_path = bag_paths[bag_index]

    print(f'Class: {classes[c]}')
    print(f'Save index: {save_index}')
    print(f'Bag path: {bag_path}')
    print()
    selected_frames = run_select_frames(bag_path, save_dir, save_index, load_frames_from_file=True)

    use_left_hand = re.search(r'left', bag_path) is not None
    print(f'Use left hand: {use_left_hand}')
    try:
        print(f'Start pose extraction for {bag_path}')
        sys.stdout.flush()
        run_pose_extraction(bag_path, selected_frames, save_dir, save_index, use_left_hand=use_left_hand)
    except Exception as e:
        print(e)
    finally:
        print('*** DONE ***')
        print()
        sys.stdout.flush()

    log.close()
    exit()
    # ******


    # save_video_parent_dir = '../../data/video/extracted_poses/'
    # c = 0
    # save_dir = save_top_dir + classes[c] + '/'
    # save_video_dir_class = save_video_parent_dir + classes[c] + '/second/'
    # start_index = 8
    # for window_multiplier in (1, 2):
    #     save_video_dir_window = f'{save_video_dir_class}{FRAME_WINDOW_BASE_SIZE * window_multiplier}/'
    #     os.makedirs(save_video_dir_window, exist_ok=True)
    #     for i, bag_path in enumerate(bag_paths):
    #         save_index = i + start_index
    #         save_video_path = f'{save_video_dir_window}pose_{save_index}.avi'
    #         test(bag_path, save_dir, save_index, window_multiplier=window_multiplier, save_video_path=save_video_path)
    # exit()
    # **********

    bag_index = 0
    from_start_index = 0
    c = 1
    bag_path = bag_paths_select_frames[bag_index]
    if not os.path.isdir(save_top_dir):
        os.mkdir(save_top_dir)


    print(f'Bag file: {bag_path}')
    save_idx = from_start_index + bag_index
    print(f'Save index: {save_idx}')
    # print('Enter save index')
    # save_idx = input()
    # print()

    # print('Enter class.')
    # print(classes)
    # selected_class = int(input())
    for class_idx in classes.keys():
        save_dir = save_top_dir + classes[class_idx] + '/'
        if not os.path.isdir(save_dir):
            os.mkdir(save_dir)
    class_name = classes[c]
    save_dir = save_top_dir + class_name + '/'

    selected_frames = run_select_frames(bag_path, save_dir, save_idx, load_frames_from_file=False)
    print(f'Class: {class_name}')
    print(f'Done selecting frames for:\n{bag_path}')

    # test(bag_path, save_dir, save_idx, window_multiplier=1)
    # exit()
