"""
This is the main program.
Runs object detection, pose extraction and hand activity detection on a bag file.
"""

from image_handler import ImageHandler
from real_sense_stream import RealSenseStream
from pose_estimator import PoseEstimator
from hand_action_detector import HandActionDetector
import os
from numba import cuda
import pickle
import sys

LEFT_HAND_LABEL = 'left_hand'
RIGHT_HAND_LABEL = 'right_hand'
HAND_LABELS = {LEFT_HAND_LABEL, RIGHT_HAND_LABEL}

# BGR
LABELS_TO_COLOR = {
    'yellow_box': (0, 255, 246),
    'red_button': (0, 18, 255),
    'grey_box': (146, 146, 146),
    'grey_button': (86, 255, 106),
    'screw_nut': (255, 255, 0),
    'left_hand': (255, 255, 255),
    'right_hand': (0, 255, 0)
}

classes = {
        0: 'grip',
        1: 'drop'
    }

KEY_POINT_COLOR = (0, 255, 0)


def main(bag_path, save_video_path, right_action_detector, left_action_detector):
    cuda.select_device(0)

    if not os.path.isfile(bag_path):
        print(f'Cannot find path {bag_path}')
        exit(1)

    if os.path.isfile(save_video_path):
        os.remove(save_video_path)

    image_handler = ImageHandler()
    stream = RealSenseStream(bag_path)

    if os.path.isfile(save_object_detection_results):
        object_detection_results = pickle.load(open(save_object_detection_results, 'rb'))
    else:
        object_detection_results = run_object_detection(stream)
        pickle.dump(object_detection_results, open(save_object_detection_results, 'wb'))

    # Release GPU memory
    cuda.close()
    cuda.select_device(0)

    object_manipulation_detection(stream, image_handler, object_detection_results, right_action_detector, left_action_detector)


def run_object_detection(stream):
    object_detector = ObjectDetector()
    object_detection_results = []
    try:
        stream_pipe = stream.start()

        i = 0
        while True:
            success, frames = stream_pipe.try_wait_for_frames()
            if i % 50 == 0:
                print(f'Object detection frame {i}')
            if not success:
                print(f'Not success at frame {i}. Break.')
                break

            valid, depth_frame, color_frame = stream.aligned_frames(frames)
            if not valid:
                continue

            color_image = stream.frame_to_image(color_frame)
            object_detector.detect_array_img(color_image)
            object_detection_results.append(object_detector.best_detection_results())

            i += 1
    except Exception as e:
        print(e)
    finally:
        stream.stop()

    print('Object detection done.\n')
    return object_detection_results


def object_manipulation_detection(stream, image_handler, object_detection_results, right_action_detector, left_action_detector):
    if os.path.isfile(save_pose_estimation_results):
        pose_estimation_results = pickle.load(open(save_pose_estimation_results, 'rb'))
        pose_estimator = None
    else:
        pose_estimation_results = []
        pose_estimator = PoseEstimator(use_hand_rectangles=True, hand_net_resolution=HAND_NET_RESOLUTION)

    video_cap_initialized = False
    i = 0
    pred_actions = []
    try:
        stream_pipe = stream.start()
        while True:
            success, frames = stream_pipe.try_wait_for_frames()
            if not success:
                print(f'Not success at frame {i}. Break.')
                break

            valid, depth_frame, color_frame = stream.aligned_frames(frames)
            if not valid:
                continue

            color_image = stream.frame_to_image(color_frame)

            if not video_cap_initialized:
                h, w, _ = color_image.shape
                image_handler.init_video_capture(save_video_path, w, h)
                video_cap_initialized = True

            object_detection_result = object_detection_results[i]

            hand_rectangles = create_hand_rectangles(object_detection_result)
            if pose_estimator is not None:
                pose_estimator.pose_estimate(color_image, hand_rectangles)
                right_kps = pose_estimator.right_hand_points()
                left_kps = pose_estimator.left_hand_points()
                pose_estimation_results.append({'right': right_kps, 'left': left_kps})
            else:
                right_kps = pose_estimation_results[i]['right']
                left_kps = pose_estimation_results[i]['left']

            if i % 10 == 0:
                print(f'Pose estimation frame {i}')

            image_handler.set_image(color_image, save_unmodified_copy=False, convert_brg2rgb=True)

            c, closest_object, d_to_labels_center = right_action_detector.predict_action(right_kps, object_detection_result, depth_frame)
            if c is None:
                pass
            else:
                pred_actions.append((c, closest_object))
            for action_i, action in enumerate(pred_actions):
                c, closest_object = action
                if c == 0:
                    color = (0, 255, 0)
                else:
                    color = (0, 0, 255)
                image_handler.put_text(f"{classes[c]}, {closest_object}", offsetY=35*action_i, color=color)

            # *** Code for just printing the results from the hand activity classifier without
            #     filtering (except confidence threshold) ***
            # extracted_right_coords = right_action_detector.extract_coords_from_frame(right_kps)
            # right_action_detector.add_pose_frame(extracted_right_coords)
            # right_action_pred = right_action_detector.predict()
            # if right_action_pred is not None:
            #     for pred_i, pred in enumerate(right_action_pred):
            #         if pred >= ACTION_ACCURACY_LIMIT:
            #             if pred_i == 0:
            #                 color = (0, 255, 0)
            #             else:
            #                 color = (0, 0, 255)
            #             label = classes[pred_i] + ': ' + str(round(pred, 2))
            #             image_handler.put_text(label, color=color, font_scale=2, position=None, offsetX=600, offsetY=400)


            draw_hand_key_points(image_handler, right_kps, left_kps, w, h)
            add_object_boxes(image_handler, object_detection_result, d_to_labels_center=None)
            image_handler.write_to_video()

            i += 1
            sys.stdout.flush()
    except Exception as e:
        print(e)
    finally:
        stream.stop()

    if pose_estimator is not None:
        pickle.dump(pose_estimation_results, open(save_pose_estimation_results, 'wb'))


def draw_hand_key_points(image_handler, right_kps, left_kps, w, h):
    # Draw hand key points
    for kp_index in use_key_points:
        x, y, _ = right_kps[kp_index]
        image_handler.draw_point((int(x * w), int(y * h)), color=KEY_POINT_COLOR)

        x, y, _ = left_kps[kp_index]
        image_handler.draw_point((int(x * w), int(y * h)), color=KEY_POINT_COLOR)


def create_hand_rectangles(object_detection_result):
    hand_rectangles = []
    hands = []
    count = 0
    for r in object_detection_result:
        label, prob, x, y, w, h = ObjectDetector.extract_detection_result(
            r,
            make_sides_equal_for=HAND_LABELS,
            width_factor=HAND_BBOX_SCALE_FACTOR,
            height_factor=HAND_BBOX_SCALE_FACTOR
        )
        if label in HAND_LABELS:
            hands.append((label, PoseEstimator.create_hand_rectangle(x, y, w, h)))
            count += 1
            if count == 2:
                break

    return PoseEstimator.create_hand_rectangles(hands)


def add_object_boxes(image_handler, object_detection_result, d_to_labels_center=None):
    for i, r in enumerate(object_detection_result):
        label, prob, x, y, w, h = ObjectDetector.extract_detection_result(
            r,
            make_sides_equal_for=(None, ),
            width_factor=1,
            height_factor=1
        )
        if label not in HAND_LABELS:
            image_handler.draw_box(x, y, w, h, label + ' ' + str(round(prob, 2)), LABELS_TO_COLOR[label])
            x_center = int(x + w / 2)
            y_center = int(y + h / 2)
            image_handler.draw_point((x_center, y_center))

            # For showing average distance from hand key points to center of objects.
            if d_to_labels_center is not None:
                if label in d_to_labels_center:
                    d = str(round(d_to_labels_center[label], 2))
                    image_handler.put_text(d, offsetX=x_center - 60, offsetY=y_center - 55, color=(255, 255, 255))


if __name__ == '__main__':
    vid_name = 'new_depth_3'
    window_size = 32
    model_index = 1
    use_key_points = HandActionDetector.USE_KEY_POINT_INDEXES_5
    model = 'model_original_32_1-best'  # => {model_original_32_1}.hdf5
    ACTION_ACCURACY_LIMIT = 0.95

    save_object_detection_results = f'object_detections_{vid_name}.p'
    save_pose_estimation_results = f'pose_estimations_{vid_name}.p'
    model_path = f'hand_action_detector/models/model_original_{window_size}_{model_index}/{model}.hdf5'
    right_action_detector = HandActionDetector(
        classes,
        use_key_points=use_key_points,
        window_size=window_size,
        model_path=model_path)

    left_action_detector = HandActionDetector(
        classes,
        use_key_points=use_key_points,
        window_size=window_size,
        model_path=model_path
    )

    from object_detector import ObjectDetector

    bag_path_dir = '../data/bag_files/test_hand_pred/'
    save_video_parent_dir = '../data/video/'
    save_video_dir = save_video_parent_dir + 'pose_and_objects_yolo_hands/'
    if not os.path.isdir(save_video_dir):
        os.makedirs(save_video_dir, exist_ok=True)

    HAND_NET_RESOLUTION = PoseEstimator.DEFAULT_HAND_NET_RESOLUTION
    HAND_BBOX_SCALE_FACTOR = ObjectDetector.DEFAULT_SQUARE_BBOX_SCALE_FACTOR

    bag_path = f'{bag_path_dir}{vid_name}.bag'
    print(f'Running bag file: {bag_path}')
    save_video_path = f'{save_video_dir}{vid_name}_{window_size}_{model_index}_{model}_demo_activities_filtered_5.avi'
    main(bag_path, save_video_path, right_action_detector, left_action_detector)
    print('Done. Exiting...')
