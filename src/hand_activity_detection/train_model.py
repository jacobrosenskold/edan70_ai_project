"""
Script for training hand activity model_original_32_1.
"""
import pickle
import os
from hand_activity_detection import HandActivityDetector
from pose_coordinates_data_preprocessing import prepare_data
from test_model import run_test


if __name__ == '__main__':
    window_size = 32
    use_key_points = HandActivityDetector.USE_KEY_POINT_INDEXES_5

    classes = {
        0: 'grip',
        1: 'drop'
    }
    handActivityDetector = HandActivityDetector(classes, window_size)

    model_index = 1
    max_epochs = 2000
    batch_size = 256

    save_model = f'models/model_original_{window_size}_{model_index}/'
    if os.path.isdir(save_model):
        print('Model path already exist.')
        exit(1)
    else:
        os.makedirs(save_model, exist_ok=True)

    train, test, validate = prepare_data(
        window_size,
        use_key_points,
        train_on=0.7,
        batch_size=batch_size)

    handActivityDetector.fit_model(
        train,
        validate,
        max_epochs,
        batch_size,
        save_model)

    pickle.dump(test, open(f'{save_model}test_data_{model_index}.p', 'wb'))

    print('\nRunning tests...')
    run_test(test, model_index, window_size)
