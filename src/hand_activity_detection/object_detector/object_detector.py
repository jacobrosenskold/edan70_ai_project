"""
A wrapper class for YOLO.
"""
from darknet import load_net, load_meta, detect, array_to_image, detect_image


class ObjectDetector:
    DEFAULT_SQUARE_BBOX_SCALE_FACTOR = 3.5

    def __init__(self,
                 yolo_weights='/home/mas15jro/project/ai_project/yolo_cfg/stop_button_hands_best.weights',
                 yolo_cfg='/home/mas15jro/project/ai_project/yolo_cfg/stop_button_hands.cfg',
                 yolo_data='/home/mas15jro/project/ai_project/yolo_cfg/stop_button_hands.data'):
        self.__net = load_net(yolo_cfg.encode('utf-8'), yolo_weights.encode('utf-8'), 0)
        self.__meta = load_meta(yolo_data.encode('utf-8'))
        self.__detection_result = None

    def detect(self, image_path=None):
        self.__detection_result = detect(self.__net, self.__meta, image_path.encode('utf-8'))

    def detect_array_img(self, array_img):
        img, _ = array_to_image(array_img)
        self.__detection_result = detect_image(self.__net, self.__meta, img)

    def detection_results(self):
        return self.__detection_result

    def best_detection_results(self):
        results = {}
        for r in self.__detection_result:
            label = r[0].decode("utf-8")
            prob = r[1]
            if label not in results or prob > results[label][1]:
                results[label] = r

        return list(results.values())

    @staticmethod
    def extract_detection_result(result,
                                 make_sides_equal_for=(None, ),
                                 width_factor=DEFAULT_SQUARE_BBOX_SCALE_FACTOR,
                                 height_factor=DEFAULT_SQUARE_BBOX_SCALE_FACTOR,
                                 xy_center=False):
        """
        :param result:
        :param make_sides_equal_for: a set with labels
        :param width_factor:
        :param height_factor:
        :return:
        """
        label = result[0].decode("utf-8")
        prob = result[1]

        center_x = result[2][0]
        center_y = result[2][1]
        w = int(result[2][2])
        h = int(result[2][3])
        if label in make_sides_equal_for:
            w = h = max(w, h)
            w = int(w * width_factor)
            h = int(h * height_factor)

        if xy_center:
            x = center_x
            y = center_y
        else:
            x = int(center_x - w / 2)
            y = int(center_y - h / 2)

        return label, prob, x, y, w, h


if __name__ == '__main__':
    object_detector = ObjectDetector('yolo_weights/yolo_default.weights', 'yolo_cfg/yolov3.cfg', 'yolo_cfg/coco.data')