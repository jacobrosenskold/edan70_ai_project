"""
Class for hand activity detection.
Used both for training a model_original_32_1 and using an already trained model_original_32_1.
"""

from keras.models import Sequential, load_model
from keras.layers import LSTM, Dense, Dropout
from keras.callbacks import ModelCheckpoint
from keras import utils, optimizers
from real_sense_stream import RealSenseStream
import os
import pickle
import numpy as np

CONFIDENCE_LIMIT_PER_CLASS = {
    0: 0.95,
    1: 0.95
}
PREDICTION_STREAK_LIMIT_PER_CLASS = {
    0: 5,
    1: 5
}
OBJECT_DISTANCE_LIMIT = 0.09

LEFT_HAND_LABEL = 'left_hand'
RIGHT_HAND_LABEL = 'right_hand'
HAND_LABELS = {LEFT_HAND_LABEL, RIGHT_HAND_LABEL}


class HandActivityDetector:
    USE_KEY_POINT_INDEXES_21 = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20)
    USE_KEY_POINT_INDEXES_15 = (2, 3, 4, 6, 7, 8, 10, 11, 12, 14, 15, 16, 18, 19, 20)
    USE_KEY_POINT_INDEXES_5 = (4, 8, 12, 16, 20)

    def __init__(self, classes, use_key_points=USE_KEY_POINT_INDEXES_5, window_size=32, model_path=None, image_shape=(720, 1280)):
        """
        :param classes: A dictionary with classes: int -> str
        :param window_size:
        :param model_path:
        """
        if model_path is None:
            self.__model = None
        else:
            print()
            print(f'Loading model_original_32_1 at {model_path}')
            self.__model = load_model(model_path)

        # For some reason model_original_32_1 has to be loaded before ObjectDetector (darknet) is imported.
        from object_detector import ObjectDetector
        self.__hand_bbox_scale_factor = ObjectDetector.DEFAULT_SQUARE_BBOX_SCALE_FACTOR
        self.__extract_detection_results = ObjectDetector.extract_detection_result

        self.__window_size = window_size
        self.__pose_frames = []
        self.__classes = classes
        self.__use_key_points = use_key_points
        self.__object_detection_frame = None
        self.__prediction_streak_per_class = np.zeros(len(classes))
        self.__image_shape = image_shape
        self.__latest_action = (None, None)

    def fit_model(self, train, validate, epochs, batch_size, save_dir):
        """
        :param train: tuple (X_train, Y_train)
        :param validate: tuple (X_valid, Y_valid)
        :param save_dir:
        :return:
        """
        X_train, Y_train = train
        input_shape = (X_train[0].shape[0], X_train[0].shape[1])

        model = Sequential()
        model.add(LSTM(10, input_shape=input_shape, dropout=0.3, return_sequences=True))
        model.add(Dropout(0.5))
        model.add(LSTM(10, input_shape=input_shape))
        model.add(Dense(len(self.__classes), activation='softmax'))

        model.compile(loss='categorical_crossentropy',
                      optimizer=optimizers.RMSprop(learning_rate=0.001),
                      metrics=['acc'])

        if not os.path.isdir(save_dir):
            os.makedirs(save_dir, exist_ok=True)
        checkpointer = ModelCheckpoint(filepath=save_dir + 'model_original_32_1-best.hdf5', verbose=1, save_best_only=True)
        history = model.fit(
            X_train,
            Y_train,
            validation_data=validate,
            epochs=epochs,
            batch_size=batch_size,
            callbacks=[checkpointer],
            verbose=2
        )

        model.save(save_dir + 'final_model.hdf5')
        pickle.dump(history, open(save_dir + 'history.p', 'wb'))

        utils.plot_model(
            model,
            to_file=save_dir + 'model_original_32_1.png',
            show_shapes=False,
            show_layer_names=True,
            rankdir="TB",
            expand_nested=False,
            dpi=96,
        )

        self.__model = model
        print()
        model.summary()
        print('Training done.')

    def predict_action(self, pose_frame, object_detection_results, depth_frame):
        extracted_coords = self.extract_coords_from_frame(pose_frame)
        _, _, acc_distance_hand_to_object = self.__find_closest_object(extracted_coords,
                                                                                           object_detection_results,
                                                                                           depth_frame)
        self.add_pose_frame(extracted_coords)
        pred = self.predict()
        if pred is None:
            return None, None, acc_distance_hand_to_object

        c = np.argmax(pred)
        confidence = pred[c]
        action_valid, closest_object = self.__action_valid(confidence, c, extracted_coords, object_detection_results,
                                                           depth_frame)
        if action_valid:
            return c, closest_object, acc_distance_hand_to_object

        return None, None, acc_distance_hand_to_object

    def __action_valid(self, conf, c, extracted_coords, object_detection_results, depth_frame):
        if conf < CONFIDENCE_LIMIT_PER_CLASS[c]:
            self.__prediction_streak_per_class[c] = 0
            return False, None

        self.__prediction_streak_per_class[c] += 1
        if self.__prediction_streak_per_class[c] < PREDICTION_STREAK_LIMIT_PER_CLASS[c]:
            return False, None

        closest_object, distance, acc_distance_hand_to_object = self.__find_closest_object(extracted_coords, object_detection_results,
                                                              depth_frame)
        if distance > OBJECT_DISTANCE_LIMIT:
            return False, None

        latest_c, latest_object = self.__latest_action

        if c == latest_c and closest_object == latest_object:
            return False, None

        self.__latest_action = (c, closest_object)
        return True, closest_object


    def __find_closest_object(self, extracted_coords, object_detection_results, depth_frame):
        acc_distance_hand_to_object = {}
        h, w = self.__image_shape

        for i, r in enumerate(object_detection_results):
            label, prob, x, y, obj_w, obj_h = self.__extract_detection_results(
                r,
                make_sides_equal_for=HAND_LABELS,
                width_factor=self.__hand_bbox_scale_factor,
                height_factor=self.__hand_bbox_scale_factor,
                xy_center=True
            )
            if label not in HAND_LABELS:
                object_center_point = (int(x), int(y))
                kp_count = 0
                acc_distance_hand_to_object[label] = 0
                for hand_kp in extracted_coords:
                    x_kp = int(hand_kp[0] * w)
                    x_kp = min(w - 1, x_kp)
                    x_kp = max(0, x_kp)

                    y_kp = int(hand_kp[1] * h)
                    y_kp = min(h - 1, y_kp)
                    y_kp = max(0, y_kp)
                    kp_coords = (x_kp, y_kp)
                    if kp_coords == (0, 0):
                        continue

                    kp_count += 1
                    d, d1, d2 = RealSenseStream.distance_between_points(kp_coords, object_center_point, depth_frame)
                    acc_distance_hand_to_object[label] += d
                if kp_count > 0:
                    if acc_distance_hand_to_object[label] < 0:
                        acc_distance_hand_to_object[label] = 0
                    acc_distance_hand_to_object[label] /= kp_count

        # return acc_distance_hand_to_object
        closest_object = min(acc_distance_hand_to_object, key=acc_distance_hand_to_object.get)
        return closest_object, acc_distance_hand_to_object[closest_object], acc_distance_hand_to_object

    def add_pose_frame(self, extracted_pose_frame):
        # Flatten the coords
        extracted_pose_frame = [i for xy in extracted_pose_frame for i in xy]

        if len(self.__pose_frames) < self.__window_size:
            self.__pose_frames.append(extracted_pose_frame)
            if len(self.__pose_frames) == self.__window_size:
                self.__pose_frames = np.array(self.__pose_frames)
        else:
            for i in range(1, self.__window_size):
                self.__pose_frames[i - 1] = self.__pose_frames[i]
            self.__pose_frames[self.__window_size - 1] = extracted_pose_frame

    def predict(self):
        if len(self.__pose_frames) < self.__window_size:
            return None

        return self.__model.predict(np.array([self.__pose_frames]))[0]

    def predict_on(self, X):
        return self.__model.predict(X)

    def extract_coords_from_frame(self, pose_frame):
        coords = []

        for i, kp_index in enumerate(self.__use_key_points):
            x = pose_frame[kp_index][0]
            y = pose_frame[kp_index][1]
            coords.append((x, y))

        return coords