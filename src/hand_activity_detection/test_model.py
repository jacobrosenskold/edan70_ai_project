"""
Testing hand activity model_original_32_1 on test set and creating plots from training metrics.
"""
import pickle
from sklearn import metrics
from hand_activity_detection import HandActionDetector
import numpy as np
import matplotlib.pyplot as plt
import glob


def create_history_graph(model_dir):
    history = pickle.load(open(model_dir + 'history.p', 'rb'))
    print(history.history.keys())
    acc = history.history["acc"]
    val_acc = history.history["val_acc"]
    loss = history.history["loss"]
    val_loss = history.history["val_loss"]
    epochs = range(1, len(loss) + 1)

    ticks = [0]
    n_epochs = len(loss)
    n_ticks = 20
    if n_epochs < 20:
        n_ticks = n_epochs
    if n_epochs > 100:
        n_ticks = 10
    for i in range(1, n_epochs + 1):
        if i % (int(n_epochs / n_ticks)) == 0:
            ticks.append(i)

    fig = plt.figure()
    plt.plot(epochs, loss, 'r', label='Training loss')
    plt.plot(epochs, val_loss, 'b', label='Validation loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.xticks(ticks)
    plt.grid(True)
    plt.legend()
    fig.savefig(model_dir + 'loss.png')
    plt.close(fig)

    fig = plt.figure()
    plt.plot(epochs, acc, 'r', label='Training accuracy')
    plt.plot(epochs, val_acc, 'b', label='Validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.xticks(ticks)
    plt.grid(True)
    plt.legend()
    fig.savefig(model_dir + 'accuracy.png')
    plt.close(fig)


def test_model(test_data, model_dir, model_path, window_size):
    X_test, Y_test = test_data

    classes = {
        0: 'grip',
        1: 'drop'
    }

    actionDetector = HandActionDetector(classes, window_size=window_size, model_path=model_path)
    print('***')
    print(model_path)
    print('***')
    y_pred = actionDetector.predict_on(X_test)
    y_pred = np.argmax(y_pred, axis=1)

    with open(model_dir + 'info.txt', 'a') as f:
        f.write('\n\n\n')
        f.write(f'Model: {model_path}\n')
        f.write("Classification report:\n%s\n"
          % (metrics.classification_report(Y_test, y_pred)))
        f.write("Confusion matrix:\n%s" % metrics.confusion_matrix(Y_test, y_pred))


def run_test(test_data, model_index, window_size):
    model_dir = f'models/model_original_{window_size}_{model_index}/'
    create_history_graph(model_dir)
    model_paths = sorted(glob.glob(f'{model_dir}*.hdf5'))
    print(model_paths)
    for model_path in model_paths:
        test_model(test_data, model_dir, model_path, window_size)