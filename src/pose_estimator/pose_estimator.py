"""
A class for performing hand pose estimation using OpenPose.
"""
from openpose import pyopenpose as op


POSE_MODEL_PATH = '/home/mas15jro/project/ai_project/pose_models'
N_KEY_POINTS = 21
LEFT_HAND_LABEL = 'left_hand'
RIGHT_HAND_LABEL = 'right_hand'


class PoseEstimator:
    DEFAULT_HAND_NET_RESOLUTION = 480

    def __init__(self, use_hand_rectangles=True, hand_net_resolution=DEFAULT_HAND_NET_RESOLUTION):
        self.__op = None
        self.__init_op(use_hand_rectangles, hand_net_resolution)
        self.__datum = op.Datum()
        self.__use_hand_rectangles = use_hand_rectangles

    def __init_op(self, use_hand_rectangles, hand_net_resolution):
        params = {}
        params['model_folder'] = POSE_MODEL_PATH
        params['keypoint_scale'] = 3  # Normalizes the key points with respect to the shape of the image.
        params['hand'] = True
        params['hand_scale_number'] = 6
        params['hand_scale_range'] = 0.4

        if use_hand_rectangles:
            params['hand_net_resolution'] = f'{hand_net_resolution}x{hand_net_resolution}'
            params['hand_detector'] = 2
            params['body'] = 0
        else:
            params['hand_detector'] = 0
            params['net_resolution'] = '-1x272'
            params['body'] = 1

        self.__op = op.WrapperPython()
        self.__op.configure(params)
        self.__op.start()

    def pose_estimate(self, cv_image, hand_rectangles=None):
        if self.__use_hand_rectangles:
            if hand_rectangles is None:
                print('No hand rectangles was provided.')
                exit(1)

            self.__datum.handRectangles = hand_rectangles
        self.__datum.cvInputData = cv_image
        self.__op.emplaceAndPop([self.__datum])

    def output_image(self):
        return self.__datum.cvOutputData

    def hand_points(self):
        return self.__datum.handKeypoints

    def left_hand_points(self):
        """
        :return: Key points for the left hand estimated first in the image (i.e. only one hand). Returns None if no hand was found.
        """
        if self.__datum.handKeypoints[0].size == 1:
            return [0] * N_KEY_POINTS
        return self.__datum.handKeypoints[0][0]

    def right_hand_points(self):
        """
        :return: Key points for the right hand estimated first in the image (i.e. only one hand). Returns None if no hand was found.
        """
        if self.__datum.handKeypoints[1].size == 1:
            return [0] * N_KEY_POINTS
        return self.__datum.handKeypoints[1][0]

    @staticmethod
    def create_hand_rectangles(hands):
        hand_rectangles = []

        if len(hands) == 0:
            hand_rectangles.append(PoseEstimator.create_hand_rectangle(0, 0, 0, 0))
            hand_rectangles.append(PoseEstimator.create_hand_rectangle(0, 0, 0, 0))
        elif len(hands) == 1:
            if hands[0][0] == LEFT_HAND_LABEL:
                hand_rectangles.append(hands[0][1])
                hand_rectangles.append(PoseEstimator.create_hand_rectangle(0, 0, 0, 0))
            else:
                hand_rectangles.append(PoseEstimator.create_hand_rectangle(0, 0, 0, 0))
                hand_rectangles.append(hands[0][1])
        else:
            if hands[0][0] == LEFT_HAND_LABEL:
                hand_rectangles.append(hands[0][1])
                hand_rectangles.append(hands[1][1])
            else:
                hand_rectangles.append(hands[1][1])
                hand_rectangles.append(hands[0][1])

        return [hand_rectangles]

    @staticmethod
    def create_hand_rectangle(x, y, width, height):
        return op.Rectangle(x, y, width, height)
