"""
Class for streaming bag files captured with real sense camera.
"""
import pyrealsense2 as rs
import numpy as np
import os


class RealSenseStream:
    def __init__(self, bag_path):
        if not os.path.isfile(bag_path):
            print('Could not find bag file path:')
            print(bag_path)
            exit()

        self.__pipeline = None
        self.__config = rs.config()
        self.__config.enable_device_from_file(bag_path, repeat_playback=False)
        self.__align_object = rs.align(rs.stream.color)
        self.__colorizer_object = rs.colorizer()

    def start(self):
        self.__pipeline = rs.pipeline()
        profile = self.__pipeline.start(self.__config)
        device = profile.get_device()
        playback = rs.playback(device)
        playback.set_real_time(False)

        return self.__pipeline

    def stop(self):
        self.__pipeline.stop()

    def aligned_frames(self, frames):
        aligned_frames = self.__align_object.process(frames)
        depth_frame = aligned_frames.get_depth_frame()
        color_frame = aligned_frames.get_color_frame()

        if not depth_frame or not color_frame:
            return False, None, None

        return True, depth_frame, color_frame

    @staticmethod
    def frame_to_image(frame):
        return np.asanyarray(frame.get_data())

    @staticmethod
    def distance_between_points(p1, p2, depth_frame):
        x1, y1 = p1
        x2, y2 = p2

        # Provide the depth in meters at the given pixel
        d1 = depth_frame.get_distance(x1, y1)
        d2 = depth_frame.get_distance(x2, y2)

        instrin = depth_frame.profile.as_video_stream_profile().intrinsics

        # Given pixel coordinates and depth in an image with no distortion or
        # inverse distortion coefficients, compute the corresponding point in 3D space relative to the same camera
        p1_projected = rs.rs2_deproject_pixel_to_point(instrin, [x1, y1], d1)
        p2_projected = rs.rs2_deproject_pixel_to_point(instrin, [x2, y2], d2)

        x1, y1, _ = p1_projected
        x2, y2, _ = p2_projected

        d_between_points = np.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2 + (d1 - d2) ** 2)

        return d_between_points, d1, d2

