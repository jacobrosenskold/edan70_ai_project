"""
Class for showing images with OpenCV and drawing bounding boxes, dots, write text etc.
"""
import cv2
import time
import numpy as np

FONT = cv2.FONT_HERSHEY_PLAIN
FONT_SCALE = 1
TEXT_THICKNESS = 2
TEXT_OFFSET = 5
TEXT_EDGE_OFFSET = 2
BOX_THICKNESS = 2

VIDEO_FORMAT = 'MJPG'

HAND_KEY_POINTS_INTERVALS = [(0, 4), (17, 20), (13, 16), (9, 12), (5, 8)]
HAND_KEY_POINTS_LINES_FROM_ZERO = (17, 13, 9, 5)
FPS = 11  # Why not 30? I don't know...


class ImageHandler:
    def __init__(self, window_name='', n_random_colors=10):
        self.__window_name = window_name
        self.__n_random_colors = n_random_colors
        self.__window = None
        self.__fps_time = 0
        self.__frame_count = -1
        self.__image = None
        self.__unmodified_image = None
        self.__video_writer = None

        self.__colors = []
        for i in range(self.__n_random_colors):
            self.__colors.append((np.random.randint(50, 255), np.random.randint(50, 255), np.random.randint(50, 255)))

    def __del__(self):
        if self.__video_writer is not None:
            self.__video_writer.release()

        cv2.destroyAllWindows()

    def show_window(self, width=1024, height=768):
        self.__window = cv2.namedWindow(self.__window_name, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(self.__window_name, width, height)
        self.__fps_time = 0
        self.__frame_count = 0
        self.__image = None

    def set_image(self, image, convert_brg2rgb=False, save_unmodified_copy=True):
        self.__image = image

        if convert_brg2rgb:
            self.bgr_to_rgb()

        if save_unmodified_copy:
            self.__unmodified_image = np.copy(image)

    def image(self):
        return self.__image

    def show_image(self, delay=1):
        self.__frame_count += 1
        fps = 1 / (time.time() - self.__fps_time)
        cv2.putText(self.__image, 'Frame: ' + str(self.__frame_count), (20, 20), FONT, 1, (0, 0, 0), 2)
        cv2.putText(self.__image, 'FPS: ' + str(round(fps, 2)), (20, 35), FONT, 1, (0, 0, 0), 2)
        cv2.imshow(self.__window_name, self.__image)
        self.__fps_time = time.time()

        return cv2.waitKey(delay)

    def put_text(self, text, font_scale=1, offsetX=0, offsetY=0, color=(0, 0, 0), position=None):
        thickness = 2
        text_size = cv2.getTextSize(str(text), FONT, font_scale, thickness)
        text_width = text_size[0][0]
        text_height = text_size[0][1]

        if position is None:
            default_x_offset = 20
            default_y_offset = 50
        else:
            offsetX = 0
            offsetY = 0
            default_x_offset = position[0]
            default_y_offset = position[1]

        x_rect = default_x_offset - 5 + offsetX
        y_rect = default_y_offset + 10 + offsetY

        cv2.rectangle(self.__image, (x_rect, y_rect), (x_rect + text_width + 5, y_rect - text_height - 20), color, -1)
        cv2.putText(self.__image, str(text), (default_x_offset + offsetX, default_y_offset + offsetY), FONT, font_scale, (0, 0, 0), thickness)

    def inc_frame_count(self):
        self.__frame_count += 1

    def draw_box(self, x, y, w, h, label, color=(0, 0, 0)):
        cv2.rectangle(self.__image, (x, y), (x + w, y + h), color, BOX_THICKNESS)
        text_size = cv2.getTextSize(label, FONT, FONT_SCALE, TEXT_THICKNESS)
        text_width = text_size[0][0]
        text_height = text_size[0][1]

        text_rectangle_down = False

        y1_offset = BOX_THICKNESS / 2
        if y - y1_offset > 0:
            y1 = int(y - y1_offset)
        else:
            text_rectangle_down = True
            y1 = int(y + y1_offset)

        y2_offset = text_height + TEXT_OFFSET + 1
        if y - y2_offset > 0:
            y2 = y - y2_offset
        else:
            text_rectangle_down = True
            y2 = y + y2_offset

        if text_rectangle_down:
            y_label = y + TEXT_OFFSET + 10
        else:
            y_label = y - TEXT_OFFSET

        p1 = (int(x - BOX_THICKNESS / 2), y1)
        p2 = (x + text_width + TEXT_EDGE_OFFSET * 2, y2)
        cv2.rectangle(self.__image, p1, p2, color, -1)
        cv2.putText(self.__image, label, (x + TEXT_EDGE_OFFSET, y_label), FONT, FONT_SCALE, (0, 0, 0), TEXT_THICKNESS)

    def draw_point(self, point, background=False, color=(0, 0, 0), point_radius=2):
        if background:
            cv2.circle(self.__image, point, point_radius + 1, (255, 255, 255), -1)
        cv2.circle(self.__image, point, point_radius, color, -1)

    def draw_line(self, p1, p2):
        cv2.line(self.__image, p1, p2, color=(0, 255, 0), thickness=1)

    def draw_hand(self, key_points, scale_x=1, scale_y=1):
        """
        Draws the hand in key_points.
        :param key_points: key_points representing a single hand.
        :param scale_x:
        :param scale_y:
        :return:
        """
        for interval in HAND_KEY_POINTS_INTERVALS:
            for i in range(interval[0], interval[1]):
                end_pt = key_points[i + 1]
                self.__draw_line_between_key_points(key_points[i], end_pt, scale_x, scale_y)
                x, y, _ = end_pt
                self.draw_point((int(x * scale_x), int(y * scale_y)), background=False)

        for i in HAND_KEY_POINTS_LINES_FROM_ZERO:
            end_pt = key_points[i]
            self.__draw_line_between_key_points(key_points[0], end_pt, scale_x, scale_y)
            x, y, _ = end_pt
            self.draw_point((int(x * scale_x), int(y * scale_y)), background=False)

    def __draw_line_between_key_points(self, kp1, kp2, scale_x, scale_y):
        x1, y1, c1 = kp1
        x2, y2, c2 = kp2
        self.draw_line((int(x1 * scale_x), int(y1 * scale_y)), (int(x2 * scale_x), int(y2 * scale_y)))

    def bgr_to_rgb(self):
        self.__image = cv2.cvtColor(self.__image, cv2.COLOR_BGR2RGB)

    def init_video_capture(self, output_path, img_width, img_height, fps=FPS):
        fourcc = cv2.VideoWriter_fourcc(*VIDEO_FORMAT)
        self.__video_writer = cv2.VideoWriter(output_path, fourcc, fps, (img_width, img_height))

    def write_to_video(self, use_unmodified_image=False):
        if use_unmodified_image:
            self.__video_writer.write(self.__unmodified_image)
        else:
            self.__video_writer.write(self.__image)

    def frame_count(self):
        return self.__frame_count

    @staticmethod
    def wait_for_key(delay=0):
        return cv2.waitKey(delay)

    def save_img(self, path, use_unmodified_image=False):
        if use_unmodified_image:
            if self.__unmodified_image is None:
                print('No unmodified image available.')
                print('Exiting..')
                exit(1)
            else:
                cv2.imwrite(path, self.__unmodified_image)
                return

        cv2.imwrite(path, self.__image)

    def read_img(self, path):
        self.set_image(cv2.imread(path))

    def close(self):
        cv2.destroyAllWindows()
