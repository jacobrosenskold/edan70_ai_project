"""
A script for writing YOLO configuration text files.
"""

import os
import numpy as np


def main(data_path, names_path, relative_data_path_for_darknet, backup_path):
    # Create train/validate/test text files.
    sub_dirs = ['train', 'validate', 'test']
    for sub_dir in sub_dirs:
        path = data_path + sub_dir + '/images/'
        image_names = os.listdir(path)
        np.random.shuffle(image_names)
        file_path = data_path + sub_dir + '.txt'
        if os.path.isfile(file_path):
            os.remove(file_path)
        with open(file_path, 'a') as f:
            for image_name in image_names:
                image_name = image_name.replace('._', '')
                if not image_name.endswith('.png'):
                    continue
                f.write(f'{relative_data_path_for_darknet}{sub_dir}/images/{image_name}\n')


    # Count nbr of classes
    with open(names_path, 'r') as f:
        n_classes = len(f.read().split('\n'))


    # Create .data file.
    file_path = data_path + 'stop_button_hands.data'
    if os.path.isfile(file_path):
        os.remove(file_path)
    with open(file_path, 'a') as f:
        f.write(f'classes={n_classes}\n')
        f.write(f'train={relative_data_path_for_darknet}train.txt\n')
        f.write(f'valid={relative_data_path_for_darknet}validate.txt\n')
        f.write(f'names={names_path}\n')
        f.write(f'backup={backup_path}')


if __name__ == '__main__':
    backup_path = 'backup/stop_button_hands/'
    names_path = '/home/mas15jro/project/ai_project/yolo_cfg/stop_button_hands.names'
    data_path = '../../data/all_hands_and_aug/'
    relative_data_path_for_darknet = 'data/stop_button_hands/'
    main(data_path, names_path, relative_data_path_for_darknet, backup_path)
