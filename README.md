### EDAN70 Project in Computer Science ###
Artificial Intelligence project by mas15jro

### Python dependencies ###

* OpenCV
* pyopenpose (OpenPose python module)
* pyrealsense2
* darknet
* imgaug
* keras, tensorflow
* sklearn
* numpy
* matplotlib